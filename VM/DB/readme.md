# Install BD

run the script as root
- you can set DB password ;
- you can set the folder where oracle12c zip files are;
- you can set oracle SID ;

```
git clone https://gitlab.com/linuxidefix/oraclevm.git ;
cd oraclevm/VM/BD ;
sh install.sh SYSTEM /media/shared/download DB12C ;
```


Tips: if you use virtualbox shared folder
```
# mount virtualbox shared folder
mkdir -p /media/shared
mount -t vboxsf shared /media/shared
```

