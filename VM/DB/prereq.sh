#!/bin/sh

#This scripts configures Linux for Oracle 12c
echo "Configure Linux for Oracle 12c..."

#directories, assuming these exist
ORACLE_INSTALL_FILES=/tmp
SCRIPTDIR=$(pwd)
SKIP=0

if [[ "$1" != "" ]] ; then
	ORACLE_INSTALL_FILES="$1"
fi

if [[ "$2" == "skip" ]] ; then
	SKIP=1
fi

#confgure SELINUX if needed
SELINUX_CFG=/etc/selinux/config
cat $SELINUX_CFG  |grep "SELINUX=enforcing" > /dev/null
if [[ $? -eq 0 ]] ; then
	sed -e 's/SELINUX=enforcing/SELINUX=permissive/g' $SELINUX_CFG  > $SELINUX_CFG.tmp && mv $SELINUX_CFG.tmp $SELINUX_CFG    
	#Set SELINUX to Permissive in this session
	setenforce 0
fi

#install Oracle prereq RPM
#this one adds oracle-user and configure kernel values and such
[ ! -e /etc/yum.repos.d/public-yum-ol6.repo ] && [ "$SKIP" -eq 0 ] && { wget --no-check-certificate https://public-yum.oracle.com/public-yum-ol6.repo -O /etc/yum.repos.d/public-yum-ol6.repo ;  }
[ ! -e /etc/pki/rpm-gpg/RPM-GPG-KEY-oracle ]  && [ "$SKIP" -eq 0 ] &&  { wget --no-check-certificate https://public-yum.oracle.com/RPM-GPG-KEY-oracle-ol6 -O /etc/pki/rpm-gpg/RPM-GPG-KEY-oracle ; }
[ "$SKIP" -eq 0 ] && { yum -y install oracle-rdbms-server-12cR1-preinstall ; }

#unzip database files
cd $ORACLE_INSTALL_FILES
if [ -e "linuxamd64_12102_database_1of2.zip" ]; then
unzip linuxamd64_12102_database_1of2.zip
unzip linuxamd64_12102_database_2of2.zip
fi

if [ ! -e $ORACLE_INSTALL_FILES/database ] ; then
	echo ERROR: directory $ORACLE_INSTALL_FILES/database does not exist
	exit 1
fi


cd $SCRIPTDIR

echo "Linux configured." 