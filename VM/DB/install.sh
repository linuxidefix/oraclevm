#!/bin/sh

#Set up Linux and oracle
echo "Deploying Oracle..."

DEFAULT_PWD=passW0RD
DEFAULT_SID=DB12C
ORACLE_INSTALL_FILES="/tmp"

if [[ "$1" != "" ]] ; then
	DEFAULT_PWD=$1
	echo "Using user specified password"
fi

if [[ "$2" != "" ]] ; then
	ORACLE_INSTALL_FILES=$2
	echo "Using user specified oracle folder $ORACLE_INSTALL_FILES"
fi

if [[ "$3" != "" ]] ; then
	DEFAULT_SID=$3
	echo "Using user specified sid $DEFAULT_SID"
fi

if [ ! -e "$ORACLE_INSTALL_FILES" ] ; then
	$ORACLE_INSTALL_FILES does not exist
	exit 1
fi

echo "Install prereqs..."
sh prereq.sh $ORACLE_INSTALL_FILES ;
echo "Install prereqs...done."
echo "Install Oracle..."
sh install_oracle.sh $DEFAULT_PWD $ORACLE_INSTALL_FILES $DEFAULT_SID
echo "Install Oracle...done."
echo "Set up Oracle..."
sh setup_db.sh $DEFAULT_PWD
echo "Set up Oracle...done."
echo "Deploying Oracle...done."

