#!/usr/bin/env bash
####################
# as user
#
####################

installAtg(){
echo "# ATG -START- `date +'%F %T'`"

if [ $# -ne 2 ]; then
	echo "usage: $0 "
	exit 1
fi

if [[ "$1" != "" ]] ; then
	ATG_INSTALL_FILES=$1
	echo "Using user specified oracle folder $ATG_INSTALL_FILES"
fi

if [[ "$2" != "" ]] ; then
	DYNAMO_ROOT=$2
	echo "Using user specified atg folder $DYNAMO_ROOT"
fi

mkdir -p $DYNAMO_ROOT
cp installer.properties $HOME/installer.properties
cp installerCSC.properties $HOME/installerCSC.properties

PLATFORM_INSTALLER_FILE=$HOME/OCPlatform11_2.bin
ACC_INSTALLER_FILE=$HOME/OCACC11.2.bin
CRS_INSTALLER_FILE=$HOME/OCReferenceStore11.2_222RCN.bin
OCA_INSTALLER_FILE=$HOME/OCStoreAccelerator11_2.bin
CSC_INSTALLER_FILE=$HOME/OCServiceCenter11.2_224RCN.bin

PLATFORM_INSTALLER_FILE_PATCH=$DYNAMO_ROOT/patch/OCPlatform11.2_p1
CSC_INSTALLER_FILE_PATCH=$DYNAMO_ROOT/patch/Service11.2_p1

sed -i "s|DYNAMO_ROOT|$DYNAMO_ROOT|g" $HOME/installer.properties
sed -i "s|DYNAMO_ROOT|$DYNAMO_ROOT|g" $HOME/installerCSC.properties

if [ "`ls -A $DYNAMO_ROOT`" = "" ]; then
    echo "Installing ATG..."

    unzip -o $ATG_INSTALL_FILES/V78217-01.zip -d $HOME
    unzip -o $ATG_INSTALL_FILES/V78201-01.zip -d $HOME
    unzip -o $ATG_INSTALL_FILES/V78224-01.zip -d $HOME
    unzip -o $ATG_INSTALL_FILES/V100746-01.zip -d $HOME
    unzip -o $ATG_INSTALL_FILES/V78220-01.zip -d $HOME


    chmod +x $HOME/*.bin

    echo "installing $PLATFORM_INSTALLER_FILE"
    $PLATFORM_INSTALLER_FILE -i silent -f $HOME/installer.properties

    echo "installing $ACC_INSTALLER_FILE"
    $ACC_INSTALLER_FILE -i silent -f $HOME/installer.properties
    sed -i '/-Djava.naming.factory.url.pkgs=atg.jndi.url/ a\           -Datg.dynamoclientlauncher.allowremote=true \\' $DYNAMO_ROOT/home/bin/startACC

    echo "installing $CRS_INSTALLER_FILE"
    $CRS_INSTALLER_FILE -i silent -f $HOME/installer.properties

    echo "installing $OCA_INSTALLER_FILE"
    $OCA_INSTALLER_FILE -i silent -f $HOME/installer.properties

    echo "installing $CSC_INSTALLER_FILE"
    $CSC_INSTALLER_FILE -i silent -f $HOME/installerCSC.properties

    echo "Installing ATG done"

    echo "Installing patch 11.2.0.1 ..."

    unzip -o $ATG_INSTALL_FILES/11.2.0.1_patch/p23147552_112000_Generic.zip -d $DYNAMO_ROOT/patch
    unzip -o $ATG_INSTALL_FILES/11.2.0.1_patch/p23147539_112000_Generic.zip -d $DYNAMO_ROOT/patch

    { cd $PLATFORM_INSTALLER_FILE_PATCH ; } && { chmod +x bin/*.sh; } && { echo y | bin/install.sh; } && { echo "patch atg done" ; }
    { cd $CSC_INSTALLER_FILE_PATCH ; } && { chmod +x bin/*.sh; } && { echo y | bin/install.sh; } && { echo "patch csc done" ; }

    echo "Installing patch 11.2.0.1 done"

else
    echo "ATG has been installed."
fi

echo "# ATG -END- `date +'%F %T'`"
}
