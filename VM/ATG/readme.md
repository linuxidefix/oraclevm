Install ATG 11.2
================
================

run the script as a simple user you want
- you can set the folder where atg install zip files are;
- you can set the folder where you want to install atg ;


```
git clone https://gitlab.com/linuxidefix/oraclevm.git ;
cd oraclevm/VM/ATG ;
source install_atg.sh
installAtg /media/shared/download/atg11.2 /data/project/products/atg ;
```

Tips: if you use virtualbox shared folder
```
# mount virtualbox shared folder
mkdir -p /media/shared
mount -t vboxsf shared /media/shared
```
