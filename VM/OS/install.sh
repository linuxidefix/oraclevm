####################
# install.sh
####################

echo "running $0 at `date +"%F %T"`"
date

source ./install_system.sh ;
source ./install_specific.sh ;
source ./install_softs.sh ;
source ./install_clean.sh ;

installSystemAll
installSoftsAll
installSpecificAll
installCleanAll

echo "ending $0 at `date +"%F %T"`"
