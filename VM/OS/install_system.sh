####################
#
####################

echo "running $0"


installHost() {
echo "# installHost -START- `date +'%F %T'`"

if [ -z "`grep $HOSTNAME /etc/hosts`" ] ; then
	echo 127.0.0.1   $HOSTNAME VM BD >> /etc/hosts ;
	echo \:\:1   $HOSTNAME VM BD >> /etc/hosts ;
fi

}

installDisableFirewall(){
echo "# installDisableFirewall -START- `date +'%F %T'`"

su - root -c "service iptables stop"
su - root -c "chkconfig iptables off"
su - root -c "sed -i 's/SELINUX=enforcing/SELINUX=disabled/g' /etc/selinux/config"
su - root -c "setenforce 0"
}

installSystemEcho(){
	echo "`date +"%F %T"`: $0 says hello to $USER"
}

installSystemAll(){
	installSystemEcho
	installHost
	installDisableFirewall
}

