####################
# as root
# 
####################


installClean(){
	echo "# Clean -START- `date +'%F %T'`"
	rm -rf /tmp/* 2>/dev/null
	for user in `ls /home` root ; do
		su - $user -c "rm -rf \$HOME/.bash_history" ;
	done
	history -c
	echo "# Clean -END- `date +'%F %T'`"
}

installCleanFull(){
echo "# CleanFull -START- `date +'%F %T'`"
clean
yum clean all
echo "# CleanFull -END- `date +'%F %T'`"
}


installCleanEcho(){
	echo "$0 says Hello to $USER"
}

installCleanAll(){
	installCleanEcho
	installClean
}
