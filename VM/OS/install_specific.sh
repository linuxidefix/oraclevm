####################
# install_specific
# as root
####################



installCreateSharedFolder(){
echo "create shared folder"
su - root -c "mkdir -p /media/shared ;" 
}

installCreateDataFolder(){
echo "create /data folder"
su - root -c "mkdir -p /data; chmod a+rw /data ;" 
}

installAddSshKey(){
echo "add ssh keys"

for user in `ls /home` root
do
	if [ "$user" = "root" ] ; then
		public_key="ssh-dss AAAAB3NzaC1kc3MAAACBAOZ1vsMbdeR3zRhwKhYSVdoswkeeNSrSFacKA7dEICAdCDtktp/IU0Hhmp+vWh9xfxylOQ+qdvfWfOlUWOJ7p5PFGzUZluUany1eOIdbvc7flTh++Uin7oqls8U66GMtku9CLnCMBwGfMPK+CKVRQXeKjhk44hf2AO8vyP/xgVyxAAAAFQCVskTyfEL3/UIFlHx44I1BtSeK+QAAAIEA10F7pqlzD89MTdkEADx1EkDHeuZP6RaVZzDfz9neVClpXoNQTEUe0kMaaz2ovRncMV2whEH1Gd09voz7vmt5EnRH7bUtijjt9LbJ+/Fg8M8rx3dGsH/KipgJNbuvsr+gdJeKBgQAaE01aq/X/wQRS5LGnpoaOwfZBA2JPfDhg+0AAACBAIk4mwXvYB8fqpqmoKFd/l+TyW5ehuf/TgTPaiSlJ2lz6zzRCfw1e5fu2LnZ0lTXo4hgvneIV+PjIu7UqzGUAtCMMjgZddEj9mOaEPfRl1Odx5iecEBQdrB2gngs3V3iHaKVa8NrHwmHZs1tFm+Nwy55UBXWDpPC/GrOqh0cewV8 $user@$HOSTNAME"
	else
		public_key="ssh-dss AAAAB3NzaC1kc3MAAACBANFOzpouJXe+yBIvmxv5o0rw0efvv7opH8V0QaVsMcj81AfBRTirPj6g2mXvBY3H45UV6P2FTZCjHFZ/htygFF0TXteSDkw/M7z9LjfmEnREAJ0nbNe6wPLeLJ7/KfP5Pu2LjDefI5gft4y/kKxr6p7HkChESg9x5ruQ55ZZrTADAAAAFQDNFuB2gTQ24/X9O85umoQne6KcWQAAAIAYvhEGkN+F9GKkiwQ5fe3tcl5WHzloeiSFITaJ2LW7VPXtWdX5KYQRYziZxOg+nfzBAue1sZae2qrFOfVDhICKFZ3muNSxJXkofXEPxTrfu1ldbEmgDKDx0p7dIcoHNiEw5Ozx1csiqj7e3ZxYCZNslnd7ByvMuiokpQom+CiaiAAAAIEAjp9E+vflE9G/a+iJMGCHrt7aNjS37Q7WTaLb6dOV8qdVshtsu47fABwkHt0Z4YPNxzNh/G+3XpJfiGhhC1mOxrdo3gGlwk4/qkH1rpT6IsZJy+ER+Z8dkREc01zQLP6rmP++uxfbr0o/P1V00tXeaqB1qeaJK+RRgJS9lpQf5Po= $user@$HOSTNAME"
	fi
	su - $user -c "mkdir -p \$HOME/.ssh ; echo $public_key >> \$HOME/.ssh/authorized_keys ; chmod 600 \$HOME/.ssh/authorized_keys ;"
done

}

installCreateMotd(){
echo "author=K.H" > /tmp/motd
echo "version=`date "+%F-%T"`" >> /tmp/motd
cat <<EOT >> /tmp/motd
██████╗ ███████╗██╗   ██╗
██╔══██╗██╔════╝██║   ██║
██║  ██║█████╗  ██║   ██║
██║  ██║██╔══╝  ╚██╗ ██╔╝
██████╔╝███████╗ ╚████╔╝ 
╚═════╝ ╚══════╝  ╚═══╝  
                         


EOT

su - root -c "cat /tmp/motd > /etc/motd"
rm -rf /tmp/motd
}

installSpecificEcho(){
	echo "`date +"%F %T"`: $0 says hello to $USER"
}


installSpecificAll(){
	installSpecificEcho
	installCreateDataFolder
	installCreateSharedFolder
	installAddSshKey
	installCreateMotd
}

