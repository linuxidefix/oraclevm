Oracle Linux
=============
=============

Install OS
==============
 - oracle linux 6.8 / 50go / 4096 RAM
 - select french keyboard
 - choose your hostname (ex: vdatg) and click "configure network" / edit eth0 / click "connect automatically"
 - select desktop configuration
 - choose swap 6Go/ ext3

 - create "developer" user

Manual configuration
====================
- DoubleClick to open desktop windows
- Screensaver off
- Enable Automatic connection to the network
- Disable firewall
- Update System
- Install VirtualBox Addons

```
yum -y update
yum -y install dos2unix
yum -y install kernel-uek-devel-$(uname -r)
```


Configuring OS
====================
Go to and download as zip

```
# as root
wget https://gitlab.com/linuxidefix/oraclevm/repository/archive.zip?ref=master -O oraclevm-master.zip
unzip oraclevm*.zip ; rm -f oraclevm*.zip ; cd oraclevm*;
find . -name "*.sh" -exec dos2unix {} \;
cd VM/OS ;
sh install.sh
```
