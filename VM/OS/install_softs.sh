####################
# as root or user ( required root password)
# 
####################


installDos2Unix(){
echo "# dos2unix -START- `date +'%F %T'`"
su - root -c "yum -y install dos2unix"
echo "# dos2unix -END- `date +'%F %T'`"
}

installGit(){
echo "# GIT -START- `date +'%F %T'`"

current_dir="`pwd`"
su - root -c "yum -y install curl-devel expat-devel gettext-devel openssl-devel zlib-devel"
su - root -c "yum -y install gcc perl-ExtUtils-MakeMaker"

cd /tmp
wget https://www.kernel.org/pub/software/scm/git/git-2.10.0.tar.gz -O /tmp/git.tar.gz
tar xzf /tmp/git.tar.gz
cd /tmp/git-2.*
make prefix=/usr/local/git all
su - root -c "cd /tmp/git-2.* ; make prefix=/usr/local/git install"
su - root -c "echo 'export PATH=\$PATH:/usr/local/git/bin' >> /etc/bashrc"
source /etc/bashrc
rm -rf /tmp/git*
cd $current_dir
echo "# GIT -END- `date +'%F %T'`"
}

installBashCompletion(){
echo "# BASH COMPLETION -START- `date +'%F %T'`"

# install bash completion 
wget http://dl.fedoraproject.org/pub/epel/6/i386/bash-completion-1.3-7.el6.noarch.rpm -O /tmp/bash-completion.rpm
su - root -c "rpm -ivh /tmp/bash-completion.rpm"

# add git bash completion 
su - root -c "mkdir -p /etc/bash_completion.d ; wget https://raw.githubusercontent.com/git/git/master/contrib/completion/git-completion.bash -O /etc/bash_completion.d/git-completion.bash"

echo "# BASH COMPLETION -END- `date +'%F %T'`"
}


installJavaSDK(){
echo "# JAVA -START- `date +'%F %T'`"
mkdir -p /usr/java && chmod -R a+w /usr/java

# Downloading Java 6
wget http://www.reucon.com/cdn/java/jdk-6u45-linux-x64.bin -O /tmp/jdk-6-linux-x64.bin
chmod u+x /tmp/jdk-6-linux-x64.bin && currentdir=`pwd` && cd /usr/java && /tmp/jdk-6-linux-x64.bin && cd $currentdir
rm -rf /tmp/jdk-6-linux-x64.bin

# Downloading Java 7
wget https://www.reucon.com/cdn/java/jdk-7u80-linux-x64.tar.gz -O /tmp/jdk-7.tar.gz
tar -xzf /tmp/jdk-7.tar.gz -C /usr/java && rm -rf /tmp/jdk-7.tar.gz
# link to java 7 by default
rm -f /usr/java/latest && currentdir=`pwd`&& cd /usr/java && ln -s /usr/java/jdk1.7.0_80 /usr/java/latest && cd $currentdir

# Downloading Java 8
wget https://www.reucon.com/cdn/java/jdk-8u112-linux-x64.tar.gz -O /tmp/jdk-8.tar.gz
tar -xzf /tmp/jdk-8.tar.gz -C /usr/java && rm -rf /tmp/jdk-8.tar.gz

alternatives --install /usr/bin/java jar /usr/java/latest/bin/java 200000
alternatives --install /usr/bin/javaws javaws /usr/java/latest/bin/javaws 200000
alternatives --install /usr/bin/javac javac /usr/java/latest/bin/javac 200000
echo 'export JAVA_HOME=/usr/java/latest' >> /etc/bashrc
echo 'export PATH=$JAVA_HOME/bin:$PATH' >> /etc/bashrc

source /etc/bashrc && echo "JAVA JDK installed: `java -version`"
echo "# JAVA -END- `date +'%F %T'`"
}

installSVN(){
echo "# SVN -START- `date +'%F %T'`"

wget http://opensource.wandisco.com/centos/6/svn-1.9/RPMS/x86_64/serf-1.3.7-1.x86_64.rpm -O /tmp/svn-serf.rpm
wget http://opensource.wandisco.com/centos/6/svn-1.9/RPMS/x86_64/subversion-1.9.4-3.x86_64.rpm -O /tmp/svn-subversion.rpm
rpm -ivh /tmp/svn-*.rpm && rm -rf /tmp/svn-*.rpm

echo "# SVN -END- `date +'%F %T'`"
}


installAnt(){
echo "# ANT -START- `date +'%F %T'`"

su - root -c "cd /tmp ;  wget -q http://archive.apache.org/dist/ant/binaries/apache-ant-1.9.7-bin.tar.gz ;  tar -xzf apache-ant-*-bin.tar.gz ;  cp -r apache-ant-1.9.7 /opt/ant ;  rm -rf /tmp/apache-ant-*"
su - root -c "echo 'export ANT_HOME=/opt/ant '>> /etc/bashrc"
su - root -c "echo 'export PATH=\$ANT_HOME/bin:\$PATH ' >> /etc/bashrc"
source /etc/bashrc

echo "# ANT -END- `date +'%F %T'`"
}

installGradle(){
echo "# GRADLE -START- `date +'%F %T'`"

wget https://downloads.gradle.org/distributions/gradle-3.0-bin.zip -O /tmp/gradle-bin.zip && unzip -q /tmp/gradle-bin.zip -d /tmp && mv /tmp/gradle-3.0 /opt/gradle && rm -f /tmp/gradle-bin.zip

# Set Appropriate Environmental Variables
su - root -c "echo 'export GRADLE_HOME=/opt/gradle '>> /etc/bashrc"
su - root -c "echo 'export PATH=\$GRADLE_HOME/bin:\$PATH ' >> /etc/bashrc"
source /etc/bashrc

echo "# GRADLE -END- `date +'%F %T'`"

}

installMaven(){
echo "# MAVEN -START- `date +'%F %T'`"

wget http://apache.crihan.fr/dist/maven/maven-3/3.3.9/binaries/apache-maven-3.3.9-bin.tar.gz -O /tmp/maven.tar.gz && tar -xzf /tmp/maven.tar.gz -C /tmp && mv /tmp/apache-maven-3.3.9 /opt/maven && rm -f /tmp/maven.tar.gz 
# Set Appropriate Environmental Variables
su - root -c "echo 'export MAVEN_HOME=/opt/maven '>> /etc/bashrc"
su - root -c "echo 'export PATH=\$MAVEN_HOME/bin:\$PATH ' >> /etc/bashrc"
source /etc/bashrc

echo "# MAVEN -END- `date +'%F %T'`"

}

installFlash(){
echo "# flash -START- `date +'%F %T'`"

if [ $USER = "root" ]; then
user=developer
else
user=$USER
fi

su - $user -c "wget https://fpdownload.adobe.com/get/flashplayer/pdc/11.2.202.632/install_flash_player_11_linux.x86_64.tar.gz -O /tmp/install_flash_player_11_linux.x86_64.tar.gz" ;
su - $user -c "tar -xzf /tmp/install_flash_player_11_linux.x86_64.tar.gz  -C /tmp" ;
su - $user -c "mkdir -p \$HOME/.mozilla/plugins" ;
su - $user -c "cp /tmp/libflashplayer.so \$HOME/.mozilla/plugins" ;
su - $user -c "chmod a+x \$HOME/.mozilla/plugins/libflashplayer.so" ;


echo "# flash -END- `date +'%F %T'`"
}


installChrome(){
echo "# google chrome -START- `date +'%F %T'`"

su - root -c "wget https://chrome.richardlloyd.org.uk/install_chrome.sh -O /tmp/install_chrome.sh"
su - root -c "cd /tmp; chmod u+x install_chrome.sh"
su - root -c "cd /tmp; ./install_chrome.sh -q -f -d"

# source: https://chrome.richardlloyd.org.uk

echo "# google chrome -END- `date +'%F %T'`"
}



installOracle12CPrereq(){
echo "# Oracle12CPrereq -START- `date +'%F %T'`"

su - root -c "yum -y install oracle-rdbms-server-12cR1-preinstall"

echo "# Oracle12CPrereq -END- `date +'%F %T'`"
}

installEcho(){
	echo "`date +"%F %T"`: $0 says hello to $USER"
}

installSoftsAll(){
	installEcho
	installDos2Unix
	installGit
	installBashCompletion
	installJavaSDK
	installSVN
	installAnt
	installGradle
	installMaven
	installChrome
	installFlash
	installOracle12CPrereq
}
