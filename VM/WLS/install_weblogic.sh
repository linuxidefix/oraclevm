####################
# as user
# 
####################



installWeblogic(){
echo "# weblogic -START- `date +'%F %T'`"

if [ $# -ne 2 ]; then
	echo "usage: $0 WEBLOGIC_JAR_FOLDER MW_HOME"
	exit 1
fi

if [[ "$1" != "" ]] ; then
	WEBLOGIC_INSTALL_FILES=$1
	echo "Using user specified oracle folder $WEBLOGIC_INSTALL_FILES"
fi

if [[ "$2" != "" ]] ; then
	MW_HOME=$2
	echo "Using user specified weblogic folder $MW_HOME"
fi

mkdir -p $MW_HOME


echo "inventory_loc=$MW_HOME/.inventory" > /tmp/oraInst.loc

#SILENT FILE
echo "
[ENGINE]

#DO NOT CHANGE THIS.
Response File Version=1.0.0.0.0

[GENERIC]

#The oracle home location. This can be an existing Oracle Home or a new Oracle Home
ORACLE_HOME=$MW_HOME

#Set this variable value to the Installation Type selected. e.g. WebLogic Server, Coherence, Complete with Examples.
INSTALL_TYPE=WebLogic Server

#Provide the My Oracle Support Username. If you wish to ignore Oracle Configuration Manager configuration provide empty string for user name.
MYORACLESUPPORT_USERNAME=

#Provide the My Oracle Support Password
MYORACLESUPPORT_PASSWORD=<SECURE VALUE>

#Set this to true if you wish to decline the security updates. Setting this to true and providing empty string for My Oracle Support username will ignore the Oracle Configuration Manager configuration
DECLINE_SECURITY_UPDATES=true

#Set this to true if My Oracle Support Password is specified
SECURITY_UPDATES_VIA_MYORACLESUPPORT=false
" > /tmp/weblo.rsp
cp $WEBLOGIC_INSTALL_FILES/fmw_12.1.3.0.0_wls.jar /tmp -v
java -jar /tmp/fmw_12.1.3.0.0_wls.jar -silent -responseFile /tmp/weblo.rsp -invPtrLoc /tmp/oraInst.loc -ignoreSysPrereqs -novalidation -jreLoc $JAVA_HOME && rm -rf /tmp/*.tmp && rm -rf /tmp/*wls*.jar

echo "# weblogic -END- `date +'%F %T'`"
}


createWeblogicDomain(){

if [ $# -ne 3 ]; then
	echo "usage: $0 MW_HOME DOMAIN_NAME ADMIN_PASSWORD"
	exit 1
fi


if [[ "$1" != "" ]] ; then
	MW_HOME=$1
	echo "Using user specified weblogic folder $MW_HOME"
fi

if [[ "$2" != "" ]] ; then
	DOMAIN_NAME=$2
	echo "Using user weblogic domain $DOMAIN_NAME"
fi

if [[ "$3" != "" ]] ; then
	ADMIN_PASSWORD=$3
	echo "Using user weblogic password $ADMIN_PASSWORD"
fi

export MW_HOME


# WLS Configuration
# -------------------------------

export DOMAIN_NAME=$DOMAIN_NAME
export ADMIN_PASSWORD
export DOMAIN_HOME=$MW_HOME/user_projects/domains/$DOMAIN_NAME
export ADMIN_PORT=7001
export ADMIN_HOST=wlsadmin
export NM_PORT=5556
export MS_PORT=8001
export CONFIG_JVM_ARGS=-Dweblogic.security.SSL.ignoreHostnameVerification=true
export PATH=$PATH:$MW_HOME/oracle_common/common/bin:$MW_HOME/wlserver/common/bin:$MW_HOME/user_projects/domains/$DOMAIN_NAME/bin:$MW_HOME
# Add files required to build this image
rm -rf /tmp/container-scripts
cp -r container-scripts /tmp/container-scripts
for file in `ls /tmp/container-scripts` ; do
	sed -i "s|/u01/oracle|$MW_HOME|g" /tmp/container-scripts/$file
done


cp -r /tmp/container-scripts/* $MW_HOME


# Configuration of WLS Domain
sh $MW_HOME/wlst $MW_HOME/create-wls-domain.py && \
    mkdir -p $MW_HOME/user_projects/domains/$DOMAIN_NAME/servers/AdminServer/security && \
    echo "username=weblogic" > $MW_HOME/user_projects/domains/$DOMAIN_NAME/servers/AdminServer/security/boot.properties && \ 
    echo "password=$ADMIN_PASSWORD" >> $MW_HOME/user_projects/domains/$DOMAIN_NAME/servers/AdminServer/security/boot.properties && \
    cp $MW_HOME/commEnv.sh $MW_HOME/wlserver/common/bin/commEnv.sh && \
	rm $MW_HOME/create-wls-domain.py $MW_HOME/jaxrs2-template.jar 

}

installEcho(){
	echo "`date +"%F %T"`: $0 says hello to $USER"
}


