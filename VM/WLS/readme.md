# Install Weblogic 12C ( 12.1.3)

run the script as a simple user you want
- you can set the folder where weblogic jar files are;
- you can set the folder where you want to install weblogic ;

```
git clone https://gitlab.com/linuxidefix/oraclevm.git ;
cd oraclevm/VM/WLS ;
source install_weblogic.sh
MW_HOME=/data/project/products/weblogic
DOMAIN_NAME=base_domain
installWeblogic /media/shared/download $MW_HOME ;
createWeblogicDomain $MW_HOME $DOMAIN_NAME MyPassw0rd ;
```

# How to start Weblogic Admin
```
sh $MW_HOME/user_projects/domains/$DOMAIN_NAME/bin/startWeblogic.sh
```

Tips: if you use virtualbox shared folder
```
# mount virtualbox shared folder
mkdir -p /media/shared
mount -t vboxsf shared /media/shared
```
